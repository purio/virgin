FROM golang:latest as builder

RUN mkdir -p /go/src/gitlab.com/purio/virgin
WORKDIR /go/src/gitlab.com/purio/virgin
RUN go get github.com/gin-gonic/gin github.com/bluele/slack github.com/gin-contrib/static github.com/gin-contrib/gzip github.com/kelseyhightower/envconfig
COPY server.go .

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo  -o virgin .

FROM alpine:3
COPY --from=builder /go/src/gitlab.com/purio/virgin/virgin /virgin
COPY --from=builder /etc/ssl/certs /etc/ssl/certs
COPY ./site /site
EXPOSE 5000/tcp
CMD ["/virgin"]
