package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/bluele/slack"
	"github.com/gin-contrib/gzip"
	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"
	"github.com/kelseyhightower/envconfig"
)

type Configuration struct {
	Debug        bool
	EnableGZIP   bool              `envconfig:"ENABLE_GZIP"`
	BasicAuth    map[string]string `envconfig:"BASIC_AUTH"`
	SlackToken   string            `envconfig:"SLACK_TOKEN"`
	SlackChannel string            `envconfig:"SLACK_CHANNEL"`
}

func main() {
	var conf Configuration
	err := envconfig.Process("virgin", &conf)
	if err != nil {
		log.Fatal(err.Error())
	}

	if conf.Debug {
		format := "Environnement:\n\tENABLE_DEBUG: %v\n\tENABLE_GZIP: %v\n\tSLACK_TOKEN: %s\n\tSLACK_CHANNEL: %s\n"
		_, err = fmt.Printf(format, conf.Debug, conf.EnableGZIP, conf.SlackToken, conf.SlackChannel)
		if err != nil {
			log.Fatal(err.Error())
		}
	}

	if conf.SlackToken != "" && conf.SlackChannel != "" {
		api := slack.New(conf.SlackToken)
		err := api.ChatPostMessage(conf.SlackChannel, "virgin server started successfully!", nil)
		if err != nil {
			panic(err)
		}
	}

	if conf.Debug {
		gin.SetMode(gin.DebugMode)

	} else {
		gin.SetMode(gin.ReleaseMode)
	}
	router := gin.Default()
	router.GET("/ping", func(context *gin.Context) {
		context.JSON(200, gin.H{"message": "pong"})
	})

	if conf.EnableGZIP {
		router.Use(gzip.Gzip(gzip.DefaultCompression))
	}

	if len(conf.BasicAuth) == 0 {
		log.Println("no Basic Auth configured")
	} else {
		log.Println(conf.BasicAuth)
		router.Use(gin.BasicAuth(conf.BasicAuth))
	}

	router.Use(static.Serve("/", static.LocalFile("./site", false)))

	router.NoRoute(func(c *gin.Context) {
		data, err := ioutil.ReadFile("./site/404.html")
		if err != nil {
			log.Fatal(err.Error())
		}
		c.Data(http.StatusNotFound, "text/html; charset=UTF-8", data)
		// c.JSON(http.StatusNotFound, gin.H{"status": 404, "error": "404, page not exists!"})
	})

	// Listen and serve on 0.0.0.0:5000
	log.Println("Listen and serve on http://0.0.0.0:5000")
	router.Run(":5000")
}
